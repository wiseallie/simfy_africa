# == Schema Information
#
# Table name: genres
#
#  id         :uuid             not null, primary key
#  name       :citext           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_genres_on_name  (name) UNIQUE
#

class Genre < ActiveRecord::Base
  include GenreAdmin

  has_many :albums,
  class_name: "Album",
  foreign_key: :genre_id,
  dependent: :destroy

  validates :name, presence: true, uniqueness:true
end
