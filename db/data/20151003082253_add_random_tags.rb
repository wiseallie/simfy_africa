class AddRandomTags < SeedMigration::Migration
  def up
    Artist.find_each do |a|
      a.tag_list.add(*Faker::Lorem.words(5))
      a.save
    end
    Album.find_each do |a|
      a.tag_list.add(*Faker::Lorem.words(5))
      a.save
    end
    Track.find_each do |a|
      a.tag_list.add(*Faker::Lorem.words(5))
      a.save
    end
  end

  def down

  end
end
