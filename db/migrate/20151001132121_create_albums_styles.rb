class CreateAlbumsStyles < ActiveRecord::Migration
  def change
    create_table :albums_styles, id: false do |t|
      t.uuid :album_id, null: false
      t.uuid :style_id, null: false
    end
    add_index :albums_styles, [:album_id, :style_id], unique: true
    add_foreign_key :albums_styles, :albums, on_delete: :cascade, on_update: :cascade, column: :album_id
    add_foreign_key :albums_styles, :styles, on_delete: :cascade, on_update: :cascade, column: :style_id
  end
end
