class CreateTrackCredits < ActiveRecord::Migration
  def change
    create_table :track_credits, id: :uuid, default: "uuid_generate_v4()" do |t|
      t.uuid :track_id, null: false
      t.uuid :credit_type_id, null: false
      t.citext :credit, null:false
      t.timestamps null: false
    end

    add_index :track_credits, :credit
    add_index :track_credits, :credit_type_id
    add_index :track_credits, :track_id
    add_index :track_credits, [:credit, :track_id, :credit_type_id], unique: true
    add_foreign_key :track_credits, :tracks, on_delete: :cascade, on_update: :cascade, column: :track_id
    add_foreign_key :track_credits, :credit_types, on_delete: :cascade, on_update: :cascade, column: :credit_type_id
  end
end
