# == Schema Information
#
# Table name: albums
#
#  id                   :uuid             not null, primary key
#  artist_id            :uuid             not null
#  genre_id             :uuid             not null
#  name                 :citext           not null
#  release_date         :date             not null
#  duration             :float            default(0.0), not null
#  tracks_count         :integer          default(0), not null
#  album_credits_count  :integer          default(0), not null
#  description          :text
#  cover                :text
#  recording_start_date :date
#  recording_end_date   :date
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#
# Indexes
#
#  index_albums_on_artist_id           (artist_id)
#  index_albums_on_genre_id            (genre_id)
#  index_albums_on_name                (name)
#  index_albums_on_name_and_artist_id  (name,artist_id) UNIQUE
#

class Album < ActiveRecord::Base
  include AlbumAdmin
  acts_as_ordered_taggable_on :tags

  mount_uploader :cover, PictureUploader

  belongs_to :artist,
  class_name: "Artist",
  foreign_key: :artist_id,
  counter_cache: :albums_count

  belongs_to :genre,
  class_name: "Genre",
  foreign_key: :genre_id

  has_many :tracks,
  class_name: "Track",
  foreign_key: :album_id,
  dependent: :destroy
  accepts_nested_attributes_for :tracks, allow_destroy: true


  has_many :album_credits,
  class_name: "AlbumCredit",
  foreign_key: :album_id,
  dependent: :destroy
  accepts_nested_attributes_for :tracks, allow_destroy: true


  has_and_belongs_to_many :styles,
   join_table: "albums_styles",
   autosave: true,
   uniq: true
   accepts_nested_attributes_for :styles, allow_destroy: true

  has_and_belongs_to_many :themes,
    join_table: "albums_themes",
    autosave: true,
    uniq: true
    accepts_nested_attributes_for :themes, allow_destroy: true

  has_and_belongs_to_many :moods,
    join_table: "albums_moods",
    autosave: true,
    uniq: true
    accepts_nested_attributes_for :moods, allow_destroy: true


  validates :name, :artist_id, :genre_id,  presence: true
  validates :name, uniqueness: {scope: :artist_id}

end
