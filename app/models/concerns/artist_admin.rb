module ArtistAdmin
  extend ActiveSupport::Concern
  included do
    rails_admin do

      label "Artists"
      navigation_label "CDs"
      weight 1

      list do
        field :picture
        field :name
        field :age
        field :gender
        field :albums_count do
          label "No. Albums"
        end
        field :tag_list do
            label "Tags"
        end
      end

      edit do
        field :name
        field :picture
        field :birthdate
        field :gender
        field :tag_list do
          label "Tags"
        end
        field :albums
      end

      nested do
        exclude_fields :albums
      end
      modal do
        exclude_fields :albums
      end
    end
  end
end
