class HomeController < ApplicationController
  skip_before_action :authenticate_user!, except: :index_signed_in
  before_action :find_artist!, only: [:artist,:album, :track ]
  before_action :find_album!, only: [:album, :track ]
  before_action :find_track!, only: [:track ]

  def index
    @artists = Artist.order('created_at desc').limit(10)
    @albums = Album.order('created_at desc').limit(10).includes(:artist)
    @tracks = Track.order('created_at desc').limit(10).includes(album: :artist)
  end

  def index_signed_in
    @artists = Artist.order('created_at desc').limit(10)
    @albums = Album.order('created_at desc').limit(10).includes(:artist)
    @tracks = Track.order('created_at desc').limit(10).includes(album: :artist)
    render :index
  end

  def search
    search_term = "%#{params[:q].to_s}%"
    @artists = Artist.order('created_at desc').where("name ILIKE :search_term", search_term: search_term ).limit(10)
    @albums = Album.order('created_at desc').where("name ILIKE :search_term", search_term: search_term ).limit(10).includes(:artist)
    @tracks = Track.order('created_at desc').where("name ILIKE :search_term", search_term: search_term ).limit(10).includes(album: :artist)
  end

  def artist
    load_similar_artists
  end

  def album
    load_similar_artists
    load_similar_albums
  end

  def track
    load_similar_artists
    load_similar_albums
    load_similar_tracks
  end

  private

  def load_similar_artists

    # find similar artists tagged with the same tags
    @similar_artists =  Artist.tagged_with(@artist.tag_list, :any => true).limit(5)
    # try find artists with albums of the same genre
    @similar_artists = Artist.joins(:albums).where(albums: {genre_id: @artist.albums.pluck(:genre_id)}).where.not(id: @artist.id).limit(5) unless @similar_artists.present?
    # just get available five artists whose names match the artist name
    unless @similar_artists.present?
      terms = []
      keys = []
      @artist.name.split(" ").each{|x| terms << "name ilike ? "; keys << "%#{x}%" }
      @similar_artists = Artist.where.not(id: @artist.id).where(terms.join(' OR '), *keys).limit(5)
    end
    # try load artists in the same age group
    @similar_artists = Artist.where.not(id: @artist.id).where("age between ? and ?", @artist.age - 5, @artist.age + 5).limit(5) unless @similar_artists.present?
    # everything else fails, just load available artists
    @similar_artists = Artist.where.not(id: @artist.id).limit(5)  unless @similar_artists.present?
  end

  def load_similar_albums
    # find similar albums tagged with the same tags
    @similar_albums =   Album.tagged_with(@album.tag_list, :any => true).limit(5)
    # try find albums with albums of the same genre
    @similar_albums = Album.where(genre_id: @album.genre_id).where.not(id: @album.id).limit(5) unless @similar_albums.present?
    # just get available five albums whose names match the album name
    unless @similar_albums.present?
      terms = []
      keys = []
      @album.name.split(" ").each{|x| terms << "name ilike ? "; keys << "%#{x}%" }
      @similar_albums = Album.where.not(id: @album.id).where(terms.join(' OR '), *keys).limit(5)
    end
    # try load albums released at similar time
    @similar_albums = Album.where.not(id: @album.id).where("release_date between ? and  ?", 2.months.since(@album.release_date), 2.months.ago(@album.release_date)).limit(5) unless @similar_albums.present?
    # everything else fails, just load available albums
    @similar_albums = Album.where.not(id: @album.id).limit(5)  unless @similar_albums.present?
  end

  def load_similar_tracks
    # find similar tracks tagged with the same tags
    @similar_tracks =   Track.tagged_with(@track.tag_list, :any => true).limit(5)

    # load tracks of the same artist
    @similar_tracks = Track.where(album_id: @artist.albums.pluck(:id)).where.not(id: @track.id).limit(5) unless @similar_tracks.present?
    # just get available five tracks whose names match the track name
    unless @similar_tracks.present?
      terms = []
      keys = []
      @track.name.split(" ").each{|x| terms << "name ilike ? "; keys << "%#{x}%" }
      @similar_tracks = Track.where.not(id: @track.id).where(terms.join(' OR '), *keys).limit(5)
    end
    # everything else fails, just load available tracks
    @similar_tracks = Track.where.not(id: @track.id).limit(5)  unless @similar_tracks.present?
  end

  def find_artist!
    @artist = Artist.find(params[:artist_id])
  end

  def find_album!
    @album = @artist.albums.find(params[:album_id])
  end

  def find_track!
    @track = @album.tracks.find(params[:track_id])
  end
end
