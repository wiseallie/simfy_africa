# == Schema Information
#
# Table name: genres
#
#  id         :uuid             not null, primary key
#  name       :citext           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_genres_on_name  (name) UNIQUE
#

FactoryGirl.define do
  factory :genre do
    name ""
  end

end
