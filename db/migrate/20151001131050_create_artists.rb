class CreateArtists < ActiveRecord::Migration
  def change
    create_table :artists, id: :uuid, default: "uuid_generate_v4()" do |t|
      t.citext :name,  null: false
      t.date :birthdate
      t.integer :age
      t.string :gender,  null: false
      t.text :picture
      t.integer :albums_count
      t.text :biography

      t.timestamps null: false
    end
    add_index :artists, :name, unique: true
  end
end
