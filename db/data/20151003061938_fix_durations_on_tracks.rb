class FixDurationsOnTracks < SeedMigration::Migration
  def up
    Track.find_each do |t|
      unless  t.duration.to_f > 0.0
        t.duration = [rand(20) + 1, rand(99) + 1].join(".").to_f
        t.save!
      end
    end

    Album.find_each do |a|
      a.duration = a.tracks.sum(:duration).to_f
      a.save!
    end
  end

  def down

  end
end
