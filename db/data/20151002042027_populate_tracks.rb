class PopulateTracks < SeedMigration::Migration
  def up
    Track.delete_all
    credit_types = CreditType.all
    Album.order(:created_at).find_each do | album|
      (rand(20) + 1).times do |x|
        begin
          track = album.tracks.create!(
          name: Faker::Company.catch_phrase,
          duration: (rand(100).to_f/rand(100).to_f).round(2),
          track_number: x + 1
          )
          puts track.name
        rescue
          retry
        end

        (rand(5) + 1).times do |y|
          begin
            track.track_credits.create!(
            credit: Faker::Name.name,
            credit_type: credit_types.sample
            )
          rescue
            retry
          end
        end
      end
    end
  end

  def down
    Track.delete_all
  end
end
