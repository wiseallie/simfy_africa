$(document).on('show.bs.tab', 'a[data-toggle="tab"]', function (e) {
  console.log(e)
  var tabPanel = $(e.target).attr('data-target') || $(e.target).attr('href');
  var url = $(e.target).attr('data-href');
  // e.target // newly activated tab
  // e.relatedTarget // previous active tab

  if(tabPanel && url){
    $(tabPanel).html("<center><i class='fa fa-spinner fa-spin'></i> Loading...</center>");
    $(tabPanel).load( url, function( response, status, xhr ) {
    if ( status == "error" ) {
      var msg = "Sorry but there was an error: ";
      $(tabPanel).html( msg + xhr.status + " " + xhr.statusText );
    }
  });
  }
})
