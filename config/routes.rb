Rails.application.routes.draw do
  get 'home/index'
  get 'home/index_signed_in'
  get 'search', to: 'home#search'
  get 'show/artists/:artist_id', to: 'home#artist', as: 'artist'
  get 'show/artists/:artist_id/albums/:album_id', to: 'home#album', as: 'album'
  get 'show/artists/:artist_id/albums/:album_id/tracks/:track_id', to: 'home#track', as: 'track'

  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'

  authenticated :user do
    root 'home#index_signed_in', as: :authenticated_root
  end

  devise_scope :user do
    root to: 'home#index'
  end

  devise_for :users

end
