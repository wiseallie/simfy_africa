module CreditTypeAdmin
  extend ActiveSupport::Concern
  included do
    rails_admin do
      label "Credit Types"
      navigation_label "Settings"
      weight 8
      list do
        field :name
      end

      edit do
        field :name
      end
    end
  end
end
