module MoodAdmin
  extend ActiveSupport::Concern
  included do
    rails_admin do
      label "Album Moods"
      navigation_label "Settings"
      weight 5
      list do
        field :name
      end

      edit do
        field :name
      end
    end
  end
end
