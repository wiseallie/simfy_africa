# == Schema Information
#
# Table name: themes
#
#  id         :uuid             not null, primary key
#  name       :citext           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_themes_on_name  (name) UNIQUE
#

class Theme < ActiveRecord::Base
  include ThemeAdmin

  has_and_belongs_to_many :albums,
  join_table: "albums_themes",
  autosave: true,
  uniq: true

  validates :name, presence: true, uniqueness:true

end
