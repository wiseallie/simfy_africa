class CreateAlbumCredits < ActiveRecord::Migration
  def change
    create_table :album_credits, id: :uuid, default: "uuid_generate_v4()" do |t|
      t.uuid :album_id, null: false
      t.uuid :credit_type_id, null: false
      t.citext :credit, null:false
      t.timestamps null: false
    end

    add_index :album_credits, :credit
    add_index :album_credits, :credit_type_id
    add_index :album_credits, :album_id
    add_index :album_credits, [:credit, :album_id, :credit_type_id], unique: true
    add_foreign_key :album_credits, :albums, on_delete: :cascade, on_update: :cascade, column: :album_id
    add_foreign_key :album_credits, :credit_types, on_delete: :cascade, on_update: :cascade, column: :credit_type_id
  end
end
