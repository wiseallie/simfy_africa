# == Schema Information
#
# Table name: styles
#
#  id         :uuid             not null, primary key
#  name       :citext           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_styles_on_name  (name) UNIQUE
#

FactoryGirl.define do
  factory :style do
    name ""
  end

end
