# == Schema Information
#
# Table name: moods
#
#  id         :uuid             not null, primary key
#  name       :citext           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_moods_on_name  (name) UNIQUE
#

class Mood < ActiveRecord::Base
  include MoodAdmin

  has_and_belongs_to_many :albums,
  join_table: "albums_moods",
  autosave: true,
  uniq: true

  validates :name, presence: true, uniqueness:true

end
