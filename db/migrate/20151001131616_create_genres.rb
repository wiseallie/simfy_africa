class CreateGenres < ActiveRecord::Migration
  def change
    create_table :genres, id: :uuid, default: "uuid_generate_v4()" do |t|
      t.citext :name, null: false

      t.timestamps null: false
    end
    add_index :genres, :name, unique: true
  end
end
