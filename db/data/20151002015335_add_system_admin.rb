class AddSystemAdmin < SeedMigration::Migration
  def up
    user = CreateAdminService.new.call
    puts 'CREATED ADMIN USER: ' << user.email
  end

  def down
    User.destroy_all
  end
end
