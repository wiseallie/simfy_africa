# == Schema Information
#
# Table name: styles
#
#  id         :uuid             not null, primary key
#  name       :citext           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_styles_on_name  (name) UNIQUE
#

class Style < ActiveRecord::Base
  include StyleAdmin

  has_and_belongs_to_many :albums,
  join_table: "albums_styles",
  autosave: true,
  uniq: true

  validates :name, presence: true, uniqueness:true

end
