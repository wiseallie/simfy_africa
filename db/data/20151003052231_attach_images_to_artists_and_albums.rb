class AttachImagesToArtistsAndAlbums < SeedMigration::Migration
  def up
    Artist.order("created_at").find_each do |a|
      unless a.picture.present?
        begin
          a.remote_picture_url =  Faker::Avatar.image
          a.save!
        rescue
          retry
        end
      end
    end

    Album.order("created_at").find_each do |a|
      unless a.cover.present?
        begin
          a.remote_cover_url =  Faker::Avatar.image
          a.save!
        rescue
          retry
        end
      end
    end
  end

  def down

  end
end
