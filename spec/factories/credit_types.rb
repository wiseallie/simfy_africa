# == Schema Information
#
# Table name: credit_types
#
#  id         :uuid             not null, primary key
#  name       :citext           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_credit_types_on_name  (name) UNIQUE
#

FactoryGirl.define do
  factory :credit_type do
    name ""
  end

end
