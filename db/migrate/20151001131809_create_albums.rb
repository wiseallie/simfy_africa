class CreateAlbums < ActiveRecord::Migration
  def change
    create_table :albums , id: :uuid, default: "uuid_generate_v4()" do |t|
      t.uuid :artist_id, null: false
      t.uuid :genre_id, null: false
      t.citext :name,  null: false
      t.date :release_date,  null: false
      t.float :duration, null:false, default: 0.0
      t.integer :tracks_count, null:false, default: 0
      t.integer :album_credits_count, null:false, default: 0
      t.text :description
      t.text :cover
      t.date :recording_start_date
      t.date :recording_end_date

      t.timestamps null: false
    end
    add_index :albums, :name
    add_index :albums, :artist_id
    add_index :albums, :genre_id
    add_index :albums, [:name, :artist_id], unique: true

    add_foreign_key :albums, :artists, on_delete: :cascade, on_update: :cascade, column: :artist_id
    add_foreign_key :albums, :genres, on_delete: :restrict, on_update: :cascade, column: :genre_id

  end
end
