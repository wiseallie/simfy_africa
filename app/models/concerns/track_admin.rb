#
#  id                  :uuid             not null, primary key
#  album_id            :uuid             not null
#  name                :citext           not null
#  duration            :float            default(0.1), not null
#  track_number        :integer          default(1), not null
#  track_credits_count :integer          default(0), not null
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#
module TrackAdmin
  extend ActiveSupport::Concern
  included do
    rails_admin do
        label "Tracks"
        navigation_label "CDs"
        weight 3

        list do
          field :album
          field :name
          field :track_number
          field :duration
          field :tag_list do
            label "Tags"
          end
        end

        edit do
          field :album
          field :name
          field :duration
          field :track_number
          field :tag_list do
              label "Tags"
          end
          field :track_credits
        end

        nested do
          exclude_fields :album, :track_credits
        end

        modal do
          exclude_fields :track_credits
        end
    end
  end
end
