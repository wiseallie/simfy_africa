class PopulateArtists < SeedMigration::Migration
  def up
    Artist.destroy_all
    (rand(20) + 10).times do |i|
      begin
      artist = Artist.create!(
        name: Faker::Name.name,
        biography: LiterateRandomizer.sentence,
        birthdate: Faker::Date.between(10.years.ago, 100.years.ago),
        remote_picture_url: Faker::Avatar.image,
        gender: Settings.GENDERS.sample
      )
      puts artist.name
    rescue
      retry
    end
    end
  end

  def down
    Artist.destroy_all
  end
end
