module StyleAdmin
  extend ActiveSupport::Concern
  included do
    rails_admin do
      label "Album Styles"
      navigation_label "Settings"
      weight 6
      list do
        field :name
      end

      edit do
        field :name
      end
    end
  end
end
