module GenreAdmin
  extend ActiveSupport::Concern
  included do
    rails_admin do

      label "Album Genres"
      navigation_label "Settings"
      weight 4

      list do
        field :name
      end

      edit do
        field :name
      end
    end
  end
end
