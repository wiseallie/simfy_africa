class AddAdminDefaults < SeedMigration::Migration
  def up
    Theme.delete_all
    Mood.delete_all
    Style.delete_all
    CreditType.delete_all
    Genre.delete_all

    puts "ADDING DEFAULT_THEMES"
    Settings.DEFAULT_THEMES.each do |name|
      Theme.create!(name: name)
    end

    puts "ADDING DEFAULT_MOODS"
    Settings.DEFAULT_MOODS.each do |name|
      Mood.create!(name: name)
    end

    puts "ADDING DEFAULT_STYLES"
    Settings.DEFAULT_STYLES.each do |name|
      Style.create!(name: name)
    end

    puts "ADDING DEFAULT_CREDIT_TYPES"
    Settings.DEFAULT_CREDIT_TYPES.each do |name|
      CreditType.create!(name: name)
    end

    puts "ADDING DEFAULT_GENRES"
    Settings.DEFAULT_GENRES.each do |name|
      Genre.create!(name: name)
    end
  end

  def down
    Theme.delete_all
    Mood.delete_all
    Style.delete_all
    CreditType.delete_all
    Genre.delete_all
  end
end
