#
#  id                  :uuid             not null, primary key
#  album_id            :uuid             not null
#  name                :citext           not null
#  duration            :float            default(0.1), not null
#  track_number        :integer          default(1), not null
#  track_credits_count :integer          default(0), not null
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#
module AlbumCreditAdmin
  extend ActiveSupport::Concern
  included do
    rails_admin do
      label "Track Credits"
      navigation_label "CDs"
      visible  false

      list do
        field :track
        field :credit_type
        field :credit
      end

      edit do
        field :track
        field :credit_type
        field :credit
      end

      nested do
        exclude_fields :track
      end

    end
  end
end
