class PopulateAlbums < SeedMigration::Migration
  def up
    Album.delete_all
    genres = Genre.all
    moods = Mood.all
    styles = Style.all
    credit_types = CreditType.all
    themes = Theme.all

    Artist.order(:created_at).find_each do | artist|
      (rand(5) + 1).times do |x|
        begin
          date = Faker::Date.between(2000.days.ago, 100.days.ago)
          start_date = rand(100).days.since(date)
          end_date = rand(50).days.since(start_date)
          album = artist.albums.create!(
          genre: genres.sample,
          name: Faker::Company.catch_phrase,
          release_date: rand(50).days.since(end_date),
          description: LiterateRandomizer.paragraphs,
          remote_cover_url: Faker::Avatar.image,
          recording_start_date: start_date,
          recording_end_date: end_date,
          moods: moods.sample(rand(5) + 1),
          themes: themes.sample(rand(5) + 1),
          styles: styles.sample(rand(5) + 1)
          )
          puts album.name
        rescue
          retry
        end

        (rand(5) + 1).times do |y|
          begin
            album.album_credits.create!(
            credit: Faker::Name.name,
            credit_type: credit_types.sample
            )
          rescue
            retry
          end
        end
      end
    end
  end
  def down
    Album.delete_all
  end
end
