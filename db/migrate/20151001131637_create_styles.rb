class CreateStyles < ActiveRecord::Migration
  def change
    create_table :styles, id: :uuid, default: "uuid_generate_v4()" do |t|
      t.citext :name,  null: false

      t.timestamps null: false
    end
    add_index :styles, :name, unique: true
  end
end
