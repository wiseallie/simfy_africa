# == Schema Information
#
# Table name: moods
#
#  id         :uuid             not null, primary key
#  name       :citext           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_moods_on_name  (name) UNIQUE
#

FactoryGirl.define do
  factory :mood do
    name ""
  end

end
