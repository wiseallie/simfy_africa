# == Schema Information
#
# Table name: themes
#
#  id         :uuid             not null, primary key
#  name       :citext           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_themes_on_name  (name) UNIQUE
#

FactoryGirl.define do
  factory :theme do
    name ""
  end

end
