class CreateCreditTypes < ActiveRecord::Migration
  def change
    create_table :credit_types, id: :uuid, default: "uuid_generate_v4()" do |t|
      t.citext :name,  null: false

      t.timestamps null: false
    end
    add_index :credit_types, :name, unique: true
  end
end
