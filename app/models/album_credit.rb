# == Schema Information
#
# Table name: album_credits
#
#  id             :uuid             not null, primary key
#  album_id       :uuid             not null
#  credit_type_id :uuid             not null
#  credit         :citext           not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
# Indexes
#
#  index_album_credits_on_album_id                                (album_id)
#  index_album_credits_on_credit                                  (credit)
#  index_album_credits_on_credit_and_album_id_and_credit_type_id  (credit,album_id,credit_type_id) UNIQUE
#  index_album_credits_on_credit_type_id                          (credit_type_id)
#

class AlbumCredit < ActiveRecord::Base
  include AlbumCreditAdmin

  belongs_to :album,
  class_name: "Album",
  foreign_key: :album_id,
  counter_cache: :album_credits_count

  belongs_to :credit_type,
  class_name: "CreditType",
  foreign_key: :credit_type_id

  validates :credit, :album_id, :credit_type_id,  presence: true
  validates :credit, uniqueness: {scope: [:album_id, :credit_type_id]}

  def to_s
    [credit, "(#{credit_type})" ].join("")
  end
end
