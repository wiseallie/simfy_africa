# == Schema Information
#
# Table name: credit_types
#
#  id         :uuid             not null, primary key
#  name       :citext           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_credit_types_on_name  (name) UNIQUE
#

class CreditType < ActiveRecord::Base
  include CreditTypeAdmin

  has_many :album_credits,
  class_name: "AlbumCredit",
  foreign_key: :credit_type_id,
  dependent: :destroy

  has_many :track_credits,
  class_name: "TrackCredit",
  foreign_key: :credit_type_id,
  dependent: :destroy

  validates :name, presence: true, uniqueness:true
end
