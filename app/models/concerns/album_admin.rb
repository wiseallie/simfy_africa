
#  id                   :uuid             not null, primary key
#  artist_id            :uuid             not null
#  genre_id             :uuid             not null
#  name                 :citext           not null
#  release_date         :date             not null
#  duration             :float            default(0.0), not null
#  tracks_count         :integer          default(0), not null
#  album_credits_count  :integer          default(0), not null
#  description          :text
#  cover                :text
#  recording_start_date :date
#  recording_end_date   :date
#  created_at           :datetime         not null
#  updated_at
module AlbumAdmin
  extend ActiveSupport::Concern
  included do
    rails_admin do

      label "Albums"
      navigation_label "CDs"
      weight 2

      list do
        field :cover
        field :name
        field :artist
        field :genre
        field :release_date
        field :duration
        field :tracks_count do
          label "No. Tracks"
        end
        field :tag_list do
          label "Tags"
        end
      end

      edit do
        field :cover
        field :name
        field :artist
        field :genre
        field :release_date
        field :tag_list do
          label "Tags"
        end
        field :description
        field :recording_start_date
        field :recording_end_date
        field :styles do
          nested_form false
          inverse_of :albums
        end

        field :themes do
          nested_form false
          inverse_of :albums
        end
        field :themes do
          nested_form false
          inverse_of :albums
        end
        field :moods do
          nested_form false
          inverse_of :albums
        end
        field :tracks do
          inverse_of :albums
        end
      end
      nested do
        exclude_fields :tracks, :artist
      end
      modal do
        exclude_fields :tracks
      end
    end
  end
end
