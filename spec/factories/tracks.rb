# == Schema Information
#
# Table name: tracks
#
#  id                  :uuid             not null, primary key
#  album_id            :uuid             not null
#  name                :citext           not null
#  duration            :float            default(0.1), not null
#  track_number        :integer          default(1), not null
#  track_credits_count :integer          default(0), not null
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#
# Indexes
#
#  index_tracks_on_name                       (name)
#  index_tracks_on_name_and_album_id          (name,album_id) UNIQUE
#  index_tracks_on_track_number               (track_number)
#  index_tracks_on_track_number_and_album_id  (track_number,album_id) UNIQUE
#

FactoryGirl.define do
  factory :track do
    album nil
name ""
duration 1.5
track_number 1
  end

end
