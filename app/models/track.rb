# == Schema Information
#
# Table name: tracks
#
#  id                  :uuid             not null, primary key
#  album_id            :uuid             not null
#  name                :citext           not null
#  duration            :float            default(0.1), not null
#  track_number        :integer          default(1), not null
#  track_credits_count :integer          default(0), not null
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#
# Indexes
#
#  index_tracks_on_name                       (name)
#  index_tracks_on_name_and_album_id          (name,album_id) UNIQUE
#  index_tracks_on_track_number               (track_number)
#  index_tracks_on_track_number_and_album_id  (track_number,album_id) UNIQUE
#

class Track < ActiveRecord::Base
  include TrackAdmin

  acts_as_ordered_taggable_on :tags

  belongs_to :album,
  class_name: "Album",
  foreign_key: :album_id,
  counter_cache: :tracks_count

  has_many :track_credits,
  class_name: "TrackCredit",
  foreign_key: :track_id,
  dependent: :destroy
  accepts_nested_attributes_for :track_credits, allow_destroy: true

  validates :album_id, :name, :duration, :track_number, presence: true
  validates :name, :track_number, uniqueness:{scope: :album_id}

  after_commit :update_album_details

  def to_label
    [track_number, name].join(" ")
  end
  private

  def update_album_details
    Album.where(id: album_id).update_all(
      duration: Track.where(album_id: album_id).sum(:duration).to_f
    )
  end

end
