class CreateAlbumsMoods < ActiveRecord::Migration
  def change
    create_table :albums_moods, id: false do |t|
      t.uuid :album_id, null: false
      t.uuid :mood_id, null: false
    end
    add_index :albums_moods, [:album_id, :mood_id], unique: true
    add_foreign_key :albums_moods, :albums, on_delete: :cascade, on_update: :cascade, column: :album_id
    add_foreign_key :albums_moods, :moods, on_delete: :cascade, on_update: :cascade, column: :mood_id
  end
end
