class CreateAlbumsThemes < ActiveRecord::Migration
  def change
    create_table :albums_themes, id: false do |t|
      t.uuid :album_id, null: false
      t.uuid :theme_id, null: false
    end
    add_index :albums_themes, [:album_id, :theme_id], unique: true
    add_foreign_key :albums_themes, :albums, on_delete: :cascade, on_update: :cascade, column: :album_id
    add_foreign_key :albums_themes, :themes, on_delete: :cascade, on_update: :cascade, column: :theme_id
  end
end
