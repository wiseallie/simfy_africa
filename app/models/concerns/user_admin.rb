#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  confirmation_token     :string
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  unconfirmed_email      :string
#  failed_attempts        :integer          default(0), not null
#  unlock_token           :string
#  locked_at
module UserAdmin
  extend ActiveSupport::Concern
  included do
    rails_admin do
      label "Users"
      navigation_label "System Admin"
      weight 9

      list do
        field :name
        field :email
        field :role
        field :sign_in_count
        field :last_sign_in_at
        field :last_sign_in_ip
      end

      edit do
        field :role
      end

    end
  end
end
