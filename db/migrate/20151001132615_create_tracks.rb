class CreateTracks < ActiveRecord::Migration
  def change
    create_table :tracks, id: :uuid, default: "uuid_generate_v4()" do |t|
      t.uuid :album_id, null:false
      t.citext :name, null:false
      t.float :duration, null:false, default: 0.1
      t.integer :track_number, null:false, default: 1
      t.integer :track_credits_count, null:false, default: 0

      t.timestamps null: false
    end
    add_index :tracks, :name
    add_index :tracks, :track_number
    add_index :tracks, [:name, :album_id], unique: true
    add_index :tracks, [:track_number, :album_id], unique: true

    add_foreign_key :tracks, :albums, on_delete: :cascade, on_update: :cascade, column: :album_id

  end
end
