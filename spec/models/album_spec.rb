# == Schema Information
#
# Table name: albums
#
#  id                   :uuid             not null, primary key
#  artist_id            :uuid             not null
#  genre_id             :uuid             not null
#  name                 :citext           not null
#  release_date         :date             not null
#  duration             :float            default(0.0), not null
#  tracks_count         :integer          default(0), not null
#  album_credits_count  :integer          default(0), not null
#  description          :text
#  cover                :text
#  recording_start_date :date
#  recording_end_date   :date
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#
# Indexes
#
#  index_albums_on_artist_id           (artist_id)
#  index_albums_on_genre_id            (genre_id)
#  index_albums_on_name                (name)
#  index_albums_on_name_and_artist_id  (name,artist_id) UNIQUE
#

require 'rails_helper'

RSpec.describe Album, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
