# == Schema Information
#
# Table name: artists
#
#  id           :uuid             not null, primary key
#  name         :citext           not null
#  birthdate    :date
#  age          :integer
#  gender       :string           not null
#  picture      :text
#  albums_count :integer
#  biography    :text
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
# Indexes
#
#  index_artists_on_name  (name) UNIQUE
#

FactoryGirl.define do
  factory :artist do
    name ""
birthdate "2015-10-01"
age 1
gender "MyString"
picture "MyText"
  end

end
