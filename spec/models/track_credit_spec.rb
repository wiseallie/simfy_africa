# == Schema Information
#
# Table name: track_credits
#
#  id             :uuid             not null, primary key
#  track_id       :uuid             not null
#  credit_type_id :uuid             not null
#  credit         :citext           not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
# Indexes
#
#  index_track_credits_on_credit                                  (credit)
#  index_track_credits_on_credit_and_track_id_and_credit_type_id  (credit,track_id,credit_type_id) UNIQUE
#  index_track_credits_on_credit_type_id                          (credit_type_id)
#  index_track_credits_on_track_id                                (track_id)
#

require 'rails_helper'

RSpec.describe TrackCredit, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
