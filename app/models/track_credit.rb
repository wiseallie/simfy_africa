# == Schema Information
#
# Table name: track_credits
#
#  id             :uuid             not null, primary key
#  track_id       :uuid             not null
#  credit_type_id :uuid             not null
#  credit         :citext           not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
# Indexes
#
#  index_track_credits_on_credit                                  (credit)
#  index_track_credits_on_credit_and_track_id_and_credit_type_id  (credit,track_id,credit_type_id) UNIQUE
#  index_track_credits_on_credit_type_id                          (credit_type_id)
#  index_track_credits_on_track_id                                (track_id)
#

class TrackCredit < ActiveRecord::Base
  include TrackCreditAdmin

  belongs_to :track,
  class_name: "Track",
  foreign_key: :track_id,
  counter_cache: :track_credits_count

  belongs_to :credit_type,
  class_name: "CreditType",
  foreign_key: :credit_type_id

  validates :credit, :track_id, :credit_type_id,  presence: true
  validates :credit, uniqueness: {scope: [:track_id, :credit_type_id]}

  def to_s
    [credit, "(#{credit_type})" ].join("")
  end
end
