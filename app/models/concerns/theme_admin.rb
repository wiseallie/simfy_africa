module ThemeAdmin
  extend ActiveSupport::Concern
  included do
    rails_admin do
      label "Album Themes"
      navigation_label "Settings"
      weight 7
      list do
        field :name
      end

      edit do
        field :name
      end
    end
  end
end
