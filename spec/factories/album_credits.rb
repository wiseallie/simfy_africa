# == Schema Information
#
# Table name: album_credits
#
#  id             :uuid             not null, primary key
#  album_id       :uuid             not null
#  credit_type_id :uuid             not null
#  credit         :citext           not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
# Indexes
#
#  index_album_credits_on_album_id                                (album_id)
#  index_album_credits_on_credit                                  (credit)
#  index_album_credits_on_credit_and_album_id_and_credit_type_id  (credit,album_id,credit_type_id) UNIQUE
#  index_album_credits_on_credit_type_id                          (credit_type_id)
#

FactoryGirl.define do
  factory :album_credit do
    album nil
credit_type nil
credit ""
  end

end
