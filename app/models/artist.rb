# == Schema Information
#
# Table name: artists
#
#  id           :uuid             not null, primary key
#  name         :citext           not null
#  birthdate    :date
#  age          :integer
#  gender       :string           not null
#  picture      :text
#  albums_count :integer
#  biography    :text
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#
# Indexes
#
#  index_artists_on_name  (name) UNIQUE
#

class Artist < ActiveRecord::Base
  include ArtistAdmin

  acts_as_ordered_taggable_on :tags

  mount_uploader :picture, PictureUploader

  has_many :albums,
  class_name: "Album",
  foreign_key: :artist_id,
  dependent: :destroy

  accepts_nested_attributes_for :albums, allow_destroy: true

  has_many :tracks,
  through: :albums

  validates :name, presence: true, uniqueness: true
  validates :gender, presence: true, inclusion: {in: Settings.GENDERS}
  before_save do |obj|
    if obj.birthdate
      obj[:age] = Time.now.year - obj.birthdate.year
    end
    true
  end


  def gender_enum
    Settings.GENDERS
  end
end
