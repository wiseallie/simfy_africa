class CreateThemes < ActiveRecord::Migration
  def change
    create_table :themes, id: :uuid, default: "uuid_generate_v4()" do |t|
      t.citext :name,  null: false

      t.timestamps null: false
    end
    add_index :themes, :name, unique: true
  end
end
