# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151001132657) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "citext"
  enable_extension "uuid-ossp"

  create_table "album_credits", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "album_id",       null: false
    t.uuid     "credit_type_id", null: false
    t.citext   "credit",         null: false
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "album_credits", ["album_id"], name: "index_album_credits_on_album_id", using: :btree
  add_index "album_credits", ["credit", "album_id", "credit_type_id"], name: "index_album_credits_on_credit_and_album_id_and_credit_type_id", unique: true, using: :btree
  add_index "album_credits", ["credit"], name: "index_album_credits_on_credit", using: :btree
  add_index "album_credits", ["credit_type_id"], name: "index_album_credits_on_credit_type_id", using: :btree

  create_table "albums", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "artist_id",                          null: false
    t.uuid     "genre_id",                           null: false
    t.citext   "name",                               null: false
    t.date     "release_date",                       null: false
    t.float    "duration",             default: 0.0, null: false
    t.integer  "tracks_count",         default: 0,   null: false
    t.integer  "album_credits_count",  default: 0,   null: false
    t.text     "description"
    t.text     "cover"
    t.date     "recording_start_date"
    t.date     "recording_end_date"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

  add_index "albums", ["artist_id"], name: "index_albums_on_artist_id", using: :btree
  add_index "albums", ["genre_id"], name: "index_albums_on_genre_id", using: :btree
  add_index "albums", ["name", "artist_id"], name: "index_albums_on_name_and_artist_id", unique: true, using: :btree
  add_index "albums", ["name"], name: "index_albums_on_name", using: :btree

  create_table "albums_moods", id: false, force: :cascade do |t|
    t.uuid "album_id", null: false
    t.uuid "mood_id",  null: false
  end

  add_index "albums_moods", ["album_id", "mood_id"], name: "index_albums_moods_on_album_id_and_mood_id", unique: true, using: :btree

  create_table "albums_styles", id: false, force: :cascade do |t|
    t.uuid "album_id", null: false
    t.uuid "style_id", null: false
  end

  add_index "albums_styles", ["album_id", "style_id"], name: "index_albums_styles_on_album_id_and_style_id", unique: true, using: :btree

  create_table "albums_themes", id: false, force: :cascade do |t|
    t.uuid "album_id", null: false
    t.uuid "theme_id", null: false
  end

  add_index "albums_themes", ["album_id", "theme_id"], name: "index_albums_themes_on_album_id_and_theme_id", unique: true, using: :btree

  create_table "artists", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.citext   "name",         null: false
    t.date     "birthdate"
    t.integer  "age"
    t.string   "gender",       null: false
    t.text     "picture"
    t.integer  "albums_count"
    t.text     "biography"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "artists", ["name"], name: "index_artists_on_name", unique: true, using: :btree

  create_table "credit_types", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.citext   "name",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "credit_types", ["name"], name: "index_credit_types_on_name", unique: true, using: :btree

  create_table "genres", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.citext   "name",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "genres", ["name"], name: "index_genres_on_name", unique: true, using: :btree

  create_table "moods", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.citext   "name",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "moods", ["name"], name: "index_moods_on_name", unique: true, using: :btree

  create_table "seed_migration_data_migrations", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "version"
    t.integer  "runtime"
    t.datetime "migrated_on"
  end

  create_table "styles", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.citext   "name",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "styles", ["name"], name: "index_styles_on_name", unique: true, using: :btree

  create_table "taggings", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "tag_id"
    t.uuid     "taggable_id"
    t.string   "taggable_type"
    t.uuid     "tagger_id"
    t.string   "tagger_type"
    t.string   "context",       limit: 128
    t.datetime "created_at"
  end

  add_index "taggings", ["tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type"], name: "taggings_idx", unique: true, using: :btree
  add_index "taggings", ["taggable_id", "taggable_type", "context"], name: "index_taggings_on_taggable_id_and_taggable_type_and_context", using: :btree

  create_table "tags", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string  "name"
    t.integer "taggings_count", default: 0
  end

  add_index "tags", ["name"], name: "index_tags_on_name", unique: true, using: :btree

  create_table "themes", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.citext   "name",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "themes", ["name"], name: "index_themes_on_name", unique: true, using: :btree

  create_table "track_credits", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "track_id",       null: false
    t.uuid     "credit_type_id", null: false
    t.citext   "credit",         null: false
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "track_credits", ["credit", "track_id", "credit_type_id"], name: "index_track_credits_on_credit_and_track_id_and_credit_type_id", unique: true, using: :btree
  add_index "track_credits", ["credit"], name: "index_track_credits_on_credit", using: :btree
  add_index "track_credits", ["credit_type_id"], name: "index_track_credits_on_credit_type_id", using: :btree
  add_index "track_credits", ["track_id"], name: "index_track_credits_on_track_id", using: :btree

  create_table "tracks", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid     "album_id",                          null: false
    t.citext   "name",                              null: false
    t.float    "duration",            default: 0.1, null: false
    t.integer  "track_number",        default: 1,   null: false
    t.integer  "track_credits_count", default: 0,   null: false
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  add_index "tracks", ["name", "album_id"], name: "index_tracks_on_name_and_album_id", unique: true, using: :btree
  add_index "tracks", ["name"], name: "index_tracks_on_name", using: :btree
  add_index "tracks", ["track_number", "album_id"], name: "index_tracks_on_track_number_and_album_id", unique: true, using: :btree
  add_index "tracks", ["track_number"], name: "index_tracks_on_track_number", using: :btree

  create_table "users", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "name",                   default: "", null: false
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        default: 0,  null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.integer  "role"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["unlock_token"], name: "index_users_on_unlock_token", unique: true, using: :btree

  create_table "version_associations", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.uuid   "version_id"
    t.string "foreign_key_name", null: false
    t.uuid   "foreign_key_id"
  end

  add_index "version_associations", ["foreign_key_name", "foreign_key_id"], name: "index_version_associations_on_foreign_key", using: :btree
  add_index "version_associations", ["version_id"], name: "index_version_associations_on_version_id", using: :btree

  create_table "versions", id: :uuid, default: "uuid_generate_v4()", force: :cascade do |t|
    t.string   "item_type",  null: false
    t.uuid     "item_id",    null: false
    t.string   "event",      null: false
    t.string   "whodunnit"
    t.text     "object"
    t.datetime "created_at"
  end

  add_index "versions", ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id", using: :btree

  add_foreign_key "album_credits", "albums", on_update: :cascade, on_delete: :cascade
  add_foreign_key "album_credits", "credit_types", on_update: :cascade, on_delete: :cascade
  add_foreign_key "albums", "artists", on_update: :cascade, on_delete: :cascade
  add_foreign_key "albums", "genres", on_update: :cascade, on_delete: :restrict
  add_foreign_key "albums_moods", "albums", on_update: :cascade, on_delete: :cascade
  add_foreign_key "albums_moods", "moods", on_update: :cascade, on_delete: :cascade
  add_foreign_key "albums_styles", "albums", on_update: :cascade, on_delete: :cascade
  add_foreign_key "albums_styles", "styles", on_update: :cascade, on_delete: :cascade
  add_foreign_key "albums_themes", "albums", on_update: :cascade, on_delete: :cascade
  add_foreign_key "albums_themes", "themes", on_update: :cascade, on_delete: :cascade
  add_foreign_key "track_credits", "credit_types", on_update: :cascade, on_delete: :cascade
  add_foreign_key "track_credits", "tracks", on_update: :cascade, on_delete: :cascade
  add_foreign_key "tracks", "albums", on_update: :cascade, on_delete: :cascade
end
